import { api_url, getData, postData } from "./index";

export const getStocks = async () => {
  return await getData(`${api_url}/stocks`);
};

export const buyStock = async (payload) => {
  return await postData(`${api_url}/stocks/buy`, payload);
};

export const sellStock = async (payload) => {
  return await postData(`${api_url}/stocks/sell`, payload);
};
