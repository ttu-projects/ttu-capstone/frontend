import { postData, api_url, getData } from "./index";

export const getCurrentUser = async () => {
  return await getData(`${api_url}/auth`);
};

export const login = async (email = "", password = "") => {
  return await postData(`${api_url}/auth/login`, { email, password });
};

export const register = async data => {
  return await postData(`${api_url}/auth/register`, data);
};

export const updateUser = async data => {
  return await postData(`${api_url}/auth`, data);
};

export const getQuestion = async data => {
  return await postData(`${api_url}/auth/get-question`, data);
};

export const resetPassword = async data => {
  return await postData(`${api_url}/auth/reset-password`, data);
};

export const verifySecurityQuestion = async data => {
  return await postData(`${api_url}/auth/question`, data);
}
