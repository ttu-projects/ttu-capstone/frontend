// export const api_url = "https://localhost:5001/api";
export const api_url = "http://ec2-3-81-202-234.compute-1.amazonaws.com/api";

export const storage_token_key = "_knotty_token";

export const randomNumber = () => (Math.random() * 2 - 1);

export async function getData(url = "") {
  try {
    const token = localStorage.getItem(storage_token_key);
    const response = await fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token ? token : ""}`
      },
    });

    return response.status === 200 || response.status === 201
      ? await response.json()
      : { error: response.statusText, ...response };
  } catch (e) {
    console.log(e);
    return { error: e.message ? e.message : e };
  }
}

export async function postData(url, data) {
  try {
    const token = localStorage.getItem(storage_token_key);

    const response = await fetch(url, {
      method: "POST",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token ? token : ""}`
      },
      body: JSON.stringify(data)
    });

    return response.status === 200 || response.status === 201
      ? await response.json()
      : { error: response.statusText, ...response };
  } catch (e) {
    console.log(e);
    return { error: e.message ? e.message : e };
  }
}
