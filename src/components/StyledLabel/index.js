import React from "react";

import { withStyles, Grid, Typography } from "@material-ui/core";

const StyledLabel = ({ label, text, classes }) => {
  return (
    <div className={classes.root}>
      <Typography className={classes.label}>{label}</Typography>
      <Typography className={classes.text}>
        <i>{text}</i>
      </Typography>
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    display: "flex",
    direction: "row",
    marginTop: 10,
    marginBottom: 10
  },
  label: {},
  text: {
    marginLeft: 10,
    opacity: 0.7
  }
}))(StyledLabel);
