import React from "react";

import {withStyles, Grid, Typography, TextField} from "@material-ui/core";

const StyledInput = ({ label, classes, onChange,type='text', onBlur, onFocus, value }) => {
  return (
    <div className={classes.root}>
      <Typography className={classes.label}>{label}</Typography>
      <TextField variant={'outlined'} type={type} className={classes.text} onChange={onChange} onBlur={onBlur} onFocus={onFocus} value={value} />
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    display: "flex",
    direction: "row",
    marginTop: 10,
    marginBottom: 10,
  },
  label: {},
  text: {
    marginLeft: 10,
    opacity: 0.7
  }
}))(StyledInput);
