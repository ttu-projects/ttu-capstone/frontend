import AppState from './app-state';

import {createStore} from "easy-peasy";

export default createStore(AppState);
