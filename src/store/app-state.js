import { action, thunk } from "easy-peasy";
import { getCurrentUser } from "../services/auth";

import { storage_token_key } from "../services";

export default {
  auth: {
    user: null,
    setUser: action((state, payload) => {
      localStorage.setItem(storage_token_key, payload.token);
      state.user = payload;
    }),
    logout: action(state => {
      localStorage.removeItem(storage_token_key);
      state.user = null;
    }),
    fetchUser: thunk(async (actions, playload) => {
      if (localStorage.getItem(storage_token_key)) {
        const { error, ...user } = await getCurrentUser();
        if (user.id) {
          actions.setUser(user);
        }
      }
    })
  },
  snack: {
    state: { open: false, message: "" },
    setSnack: action((state, payload) => {
      state.state = payload;
    }),
    closeSnack: action((state, payload) => {
      state.state = { open: false, message: "" };
    }),
    openSnack: action((state, message) => {
      state.state = { open: true, message };
    })
  },
  app: {
    ownedStocks: [],
    stocks: {},
    transactions: [],
    buyStock: action((state, payload) => {
      const { ownedStocks } = state;
      const filteredStock = ownedStocks.filter(
        stock => stock.symbol === payload.symbol
      );
      if (filteredStock.length === 0) {
        state.ownedStocks = [...ownedStocks, payload];
      } else {
        filteredStock[0].amount =
          parseInt(filteredStock[0].amount) + parseInt(payload.amount);
        state.ownedStocks = [
          filteredStock[0],
          ...ownedStocks.filter(stock => stock.symbol !== payload.symbol)
        ];
      }
    }),
    sellStock: action((state, payload) => {
      const { ownedStocks } = state;
      const filteredStock = ownedStocks.filter(
        stock => stock.symbol === payload.symbol
      );

      if (filteredStock.length > 0) {
        filteredStock[0].amount =
          parseInt(filteredStock[0].amount) - parseInt(payload.amount);

        console.log(filteredStock[0].amount);
        state.ownedStocks =
          filteredStock[0].amount > 0
            ? [
                filteredStock[0],
                ...ownedStocks.filter(stock => stock.symbol !== payload.symbol)
              ]
            : [...ownedStocks.filter(stock => stock.symbol !== payload.symbol)];
      }
    }),
    makeTransaction: action((state, payload) => {
      state.transactions = [payload, ...state.transactions];
    }),
    putStocks: action((state, payload) => {
      state.stocks = payload;
    }),
    getStock: action((state, payload) => {
      return state.stocks[payload.symbol];
    })
  }
};
