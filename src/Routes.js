import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import HomePage from "./screens/home";
import AuthPage from "./screens/auth";
import AboutPage from "./screens/about";
import NotFound from "./screens/not-found";
import { useStoreState } from "easy-peasy";
import InvestmentPortfolio from "./screens/investment-portfolio";
import Transactions from "./screens/transactions";
import AccountPage from "./screens/account";
import { Elements } from "react-stripe-elements";
import ForgotPasswordComponent from "./screens/auth/components/forgot-password";

const PrivateRoute = ({ children, ...props }) => {
  const user = useStoreState(state => state.auth.user);

  React.useEffect(() => {
    if (user) console.log(user);
  }, [user]);

  return user ? <Route {...props}>{children}</Route> : <Redirect to={"/"} />;
};

export default () => (
  <Switch>
    <Route path={"/login"}>
      <AuthPage />
    </Route>
    <Route path={"/forgot-password"}>
      <ForgotPasswordComponent />
    </Route>
    <PrivateRoute path={"about"}>
      <AboutPage />
    </PrivateRoute>
    <PrivateRoute path={"/app"} exact>
      <InvestmentPortfolio />
    </PrivateRoute>
    <PrivateRoute path={"/transactions"} exact>
      <Transactions />
    </PrivateRoute>
    <PrivateRoute path={"/account"} exact>
      <Elements>
        <AccountPage />
      </Elements>
    </PrivateRoute>
    <Route path={"/"} exact>
      <HomePage />
    </Route>
    <Route path={"/"}>
      <NotFound />
    </Route>
  </Switch>
);
