import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Grid, Typography, Button, withStyles } from "@material-ui/core";
import { useStoreState } from "easy-peasy";

const HomePage = ({ classes }) => {
  const user = useStoreState(state => state.auth.user);
  const history = useHistory();

  useEffect(() => {
    if (user && user.id) {
      history.push('app')
    }
  }, [user]);

  const navigateTo = path => () => {
    history.push(path);
  };

  return (
    <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} className={classes.banner}>
          <h1>Home Page</h1>
          <Typography>Welcome to Knotty Cash</Typography>
          <Button
            onClick={navigateTo(user ? "/app" : "/login")}
            variant={"contained"}
          >
            Start Investing with Us
          </Button>
        </Grid>
        <Grid item xs={12} container spacing={3}>
          <Grid item xs={12} sm={4} className={classes.section}>
            <img src={`${window.location.origin}/favicon.jpg`} />
            <p>
              Aliquam tempus porta libero, vel mollis arcu tempus quis. Nam
              ullamcorper venenatis tellus eu ullamcorper. Praesent eget
              imperdiet mi, quis vestibulum tellus. Suspendisse ultricies
              dapibus lectus sit amet suscipit. Proin eleifend mollis metus sed
              dignissim. Vestibulum ex velit, scelerisque quis convallis id,
              pellentesque eleifend leo
            </p>
          </Grid>
          <Grid item xs={12} sm={4} className={classes.section}>
            <img src={`${window.location.origin}/favicon.jpg`} />
            <p>
              Aliquam tempus porta libero, vel mollis arcu tempus quis. Nam
              ullamcorper venenatis tellus eu ullamcorper. Praesent eget
              imperdiet mi, quis vestibulum tellus. Suspendisse ultricies
              dapibus lectus sit amet suscipit. Proin eleifend mollis metus sed
              dignissim. Vestibulum ex velit, scelerisque quis convallis id,
              pellentesque eleifend leo
            </p>
          </Grid>
          <Grid item xs={12} sm={4} className={classes.section}>
            <img src={`${window.location.origin}/favicon.jpg`} />
            <p>
              Aliquam tempus porta libero, vel mollis arcu tempus quis. Nam
              ullamcorper venenatis tellus eu ullamcorper. Praesent eget
              imperdiet mi, quis vestibulum tellus. Suspendisse ultricies
              dapibus lectus sit amet suscipit. Proin eleifend mollis metus sed
              dignissim. Vestibulum ex velit, scelerisque quis convallis id,
              pellentesque eleifend leo
            </p>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    "& h1": {
      color: "white"
    },
    "& p": {
      color: "white"
    },
    "& button": {
      marginTop: 20
    }
  },
  banner: {
    marginTop: 15,
    height: 400,
    width: "100%",
    backgroundColor: "grey",
    backgroundImage: `url(${window.location.origin}/images/home-banner.jpg)`,
    backgroundSize: "cover",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  section: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: 20,
    "& img": {
      height: 100,
      width: 150
    },
    "& p": {
      color: "black",
      textAlign: "center"
    }
  }
}))(HomePage);
