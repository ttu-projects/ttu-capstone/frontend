import React from "react";
import { withStyles } from "@material-ui/core";

const Transactions = () => {
  return (
    <div>
      <p>transaction!</p>
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5,
    minHeight: "100vh"
  }
}))(Transactions);
