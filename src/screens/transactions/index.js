import React, { useState } from "react";
import {
  Divider,
  Grid,
  Paper,
  Typography,
  withStyles,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";

const Transactions = ({ classes, transactions }) => {
  const renderTransactions = () => {
    return (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Divider />
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="right">Type</TableCell>
                  <TableCell align="right">Date</TableCell>
                  <TableCell align="right">Stock Name</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Price</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {transactions.map(transaction => (
                  <TableRow key={transaction.timeStamp}>
                    <TableCell align="right">{transaction.type === 0 ? 'Buy': 'Sell'}</TableCell>
                    <TableCell align="right">{transaction.timeStamp}</TableCell>
                    <TableCell align="right">
                      {transaction?.stockName}
                    </TableCell>
                    <TableCell align="right">{transaction.amount}</TableCell>
                    <TableCell align="right">
                      {transaction?.price || transaction?.currentPrice}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    );
  };

  return (
    <Paper className={classes.root}>
      <Grid container>
        <Grid
          item
          xs={12}
          container
          direction={"row"}
          justify={"space-between"}
          alignItems={"center"}
        >
          <Divider />
        </Grid>
        <Grid item xs={12} container className={classes.content}>
          {transactions && transactions.length === 0 ? (
            <Typography variant={"caption"}>No transaction!</Typography>
          ) : (
            renderTransactions()
          )}
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    minHeight: "20vh"
  },
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  }
}))(Transactions);
