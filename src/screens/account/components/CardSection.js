import React from "react";
import { withStyles, Grid } from "@material-ui/core";
import {CardElement} from "react-stripe-elements";

const CardSection = () => {
  return (
    <form action="">
      <Grid container spacing={3}>
        <CardElement/>
      </Grid>
    </form>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5,
    minHeight: "100vh"
  }
}))(CardSection);
