import React, { useState } from "react";
import {
  Divider,
  Grid,
  Paper,
  Typography,
  withStyles,
  Button,
  Modal
} from "@material-ui/core";
import { CardElement, injectStripe } from "react-stripe-elements";
import { EditOutlined } from "@material-ui/icons";
import { useStoreActions, useStoreState } from "easy-peasy";
import { useHistory } from "react-router-dom";
import StyledLabel from "../../components/StyledLabel";
import StyledInput from "../../components/StyledInput";
import {updateUser} from "../../services/auth";

const AccountPage = ({ classes, stripe, elements }) => {
  const logout = useStoreActions(actions => actions.auth.logout);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const history = useHistory();
  const [edit, setEdit] = React.useState(false);
  const [addingCash, setAddingCash] = useState(false);
  const {
    investmentPortfolio: { cashAvailable, ...investmentPortfolio },
    ...user
  } = useStoreState(state => state.auth.user);
  const [amount, setAmount] = useState(10);
  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [email, setEmail] = useState(user.email);
  const setUser = useStoreActions(actions => actions.auth.setUser);

  const onLogOutPress = () => {
    logout();
    history.push("/");
    openSnack("Logged out!");
  };

  const onUpdateUserInfo = async () => {
    try {
      setEdit(false);
      console.log('firstName, ', firstName, 'lastname: ', lastName, 'email: ', email);
      const res = await updateUser({firstName, lastName, email});
      setUser(res);
      openSnack("Saved!");
    } catch (e) {
      openSnack(e?.message || e);
    }
  };

  const addCash = () => {
    setAddingCash(true);
  };

  const onSubmit = async () => {
    try {
      if (amount.length === 0 || amount < 0) {
        return openSnack("Invalid amount!");
      }
      const cardElement = elements.getElement("card");

      const paymentMethod = await stripe.createPaymentMethod({
        type: "card",
        card: cardElement,
        billing_details: { name: "Jenny Rosen" }
      });

      if (paymentMethod.error) {
        return openSnack(paymentMethod.error.message);
      }

      const res = await updateUser({firstName, lastName, email, cash: amount});
      setUser(res);
      setAddingCash(false);
      openSnack("Added Cash!");
    } catch (e) {
      openSnack(e?.message || e);
    }
  };

  return (
    <Paper className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Typography variant={"h3"}>My Account</Typography>
          <Divider />
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.section}>
            <Grid item xs={12}>
              {edit ? (
                <>
                  <StyledInput
                    placeholder={"First name"}
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                  />
                  <StyledInput
                    placeholder={"Last name"}
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                  />
                </>
              ) : (
                <StyledLabel
                  label={"Name: "}
                  text={`${user.firstName} ${user.lastName}`}
                />
              )}
            </Grid>
            <Grid item xs={12}>
              {edit ? (
                <StyledInput
                  placeholder={"Email"}
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                />
              ) : (
                <StyledLabel label={"Email: "} text={user.email} />
              )}
            </Grid>
            <Grid
              item
              xs={12}
              container
              direction={"row"}
              alignItems={"center"}
            >
              <StyledLabel
                label={"Total cash Available"}
                text={`$${cashAvailable.toFixed(2)}`}
              />
              {!edit && (
                <Button
                  onClick={addCash}
                  style={{ marginLeft: 20 }}
                  variant={"outlined"}
                >
                  Add Cash
                </Button>
              )}
            </Grid>
            <div className={classes.actions}>
              <Button
                onClick={() => {
                  setEdit(!edit);
                  if (edit) {
                    onUpdateUserInfo();
                  }
                }}
              >
                {edit ? "Save" : <EditOutlined />}
              </Button>
              {edit && <Button onClick={() => setEdit(!edit)}>Cancel</Button>}
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12} className={classes.content}>
          <Button
            variant={"outlined"}
            style={{ backgroundColor: "white" }}
            onClick={onLogOutPress}
          >
            Log Out
          </Button>
        </Grid>
      </Grid>
      <Modal open={addingCash}>
        <Paper className={classes.cardForm}>
          <form>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Typography variant={"h6"}>Add Cash</Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant={"caption"}>Amount</Typography>
                <StyledInput
                  type={"number"}
                  onChange={e => setAmount(e.target.value)}
                  value={amount}
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant={"caption"}>
                  Enter Card Information
                </Typography>
                <CardElement
                  className={classes.stripeElement}
                  style={{ base: { fontSize: "18px" } }}
                />
              </Grid>
              <Grid item xs={12} container direction={"row"}>
                <Button variant={"outlined"} onClick={onSubmit}>
                  Submit
                </Button>
                <Button
                  variant={"outlined"}
                  onClick={() => setAddingCash(false)}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Modal>
    </Paper>
  );
};

export default injectStripe(
  withStyles(theme => ({
    root: {
      padding: 20,
      margin: 5,
      minHeight: "100vh",
      backgroundColor: "rgba(0,0,0,.1)"
    },
    content: {
      marginTop: 60
    },
    section: {
      position: "relative",
      boxSizing: "border-box",
      minHeight: "20vh",
      padding: 20
    },
    actions: {
      position: "absolute",
      top: 10,
      right: 10
    },
    cardForm: {
      width: 400,
      margin: "auto",
      marginTop: 20,
      padding: 10
    },
    stripeElement: {
      borderRadius: 6,
      border: "1px solid #c2a7c2",
      boxShadow:
        "0px 3px 3px -2px rgba(0,0,0,0.2), 0px 3px 4px 0px rgba(0,0,0,0.14), 0px 1px 8px 0px rgba(0,0,0,0.12)",
      padding: 15,
      marginLeft: 10
    }
  }))(AccountPage)
);
