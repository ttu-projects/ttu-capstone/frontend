import React, { useEffect, useState } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Button,
  Typography,
  withStyles,
  Snackbar,
  createStyles,
  ButtonGroup,
} from "@material-ui/core";
import { MonetizationOnSharp } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { useStoreActions, useStoreState } from "easy-peasy";
import { makeStyles } from "@material-ui/core/styles";

const useAppBarStyles = makeStyles(theme =>
  createStyles({
    root: {
      backgroundColor: "transparent"
    }
  })
);

const Layout = ({ children, classes }) => {
  const history = useHistory();
  const user = useStoreState(state => state.auth.user);
  const snack = useStoreState(state => state.snack.state);
  const closeSnack = useStoreActions(actions => actions.snack.closeSnack);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const {fetchUser, logout} = useStoreActions(actions => actions.auth);

  const appBarClasses = useAppBarStyles();

  useEffect(() => {
    fetchUser()
  }, [fetchUser]);

  const navigateTo = path => history.push(path);

  const [anchorEl, setAnchorEl] = useState(null);
  const [open, setOpen] = useState(false);

  useEffect(() => setOpen(Boolean(anchorEl)), [anchorEl]);

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      {history.location.pathname !== '/login' && <AppBar classes={appBarClasses} position={"static"}>
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
            onClick={() => navigateTo(user ? "/app" : "/")}
          >
            <MonetizationOnSharp />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Knotty Cash
          </Typography>
          <div className={classes.floatRight}>
            {user ? (
              <>
               <ButtonGroup>
                 <Button
                   onClick={() => {
                     navigateTo("app");
                     handleClose();
                   }}
                 >
                   Dashboard
                 </Button>
                 <Button
                   onClick={() => {
                     navigateTo("account");
                     handleClose();
                   }}
                 >
                   My account
                 </Button>
                 <Button
                   onClick={() => {
                     logout();
                     openSnack("Logged out");
                     handleClose();
                   }}
                 >
                   Logout
                 </Button>
               </ButtonGroup>
              </>
            ) : (
              <Button color="secondary" onClick={() => navigateTo("login")}>
                Login
              </Button>
            )}
          </div>
        </Toolbar>
      </AppBar>}
      <div className={classes.content}>{children}</div>
      {/*<Footer/>*/}
      <Snackbar
        open={snack.open}
        message={snack.message}
        autoHideDuration={3000}
        onClose={() => closeSnack()}
      />
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    flexGrow: 1,
    "& p": {
      color: "black"
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    color: "black"
  },
  floatRight: {
    position: "absolute",
    right: theme.spacing(2),
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  content: {
    minHeight: "70vh"
  },
  title: {
    color: "black"
  },
  navBar: {
    marginLeft: 20
  }
}))(Layout);
