import React from "react";
import { withStyles, Paper, Grid } from "@material-ui/core";
import {Link} from "react-router-dom";

const Footer = ({classes}) => {
  return (
    <Paper elevation={3} className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <img className={classes.icon} src={window.location.origin + '/favicon.jpg'} />
        </Grid>
        <Grid item xs={12} sm={8} container direction={'column'}>
          <Link to={'/'}>Home</Link>
          <Link to={'/about'}>About</Link>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    minHeight: 150,
    margin: 10,
    '& a': {
      textDecoration: 'none',
      marginBottom: 10,
    }
  },
  icon: {
    height: 100, width: 120,
  }
}))(Footer);
