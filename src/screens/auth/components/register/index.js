import React from "react";
import {
  withStyles,
  Paper,
  Grid,
  TextField,
  Button,
  Typography,
  Link
} from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { useStoreActions } from "easy-peasy";
import styles from "../styles";
import {register} from "../../../../services/auth";

const RegisterComponent = ({ classes, toggleLogin }) => {
  const setUser = useStoreActions(actions => actions.auth.setUser);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const history = useHistory();
  const [state, setState] = React.useState({
    first_name: "",
    last_name: "",
    email: "",
    password: "",
    re_password: "",
    question: "",
    answer: "",
  });

  const onRegisterClick = async () => {
    if (state.first_name.length < 2) {
      return openSnack("First name is invalid");
    }

    if (state.last_name.length < 2) {
      return openSnack("Last name is invalid");
    }

    if (state.email.length < 4) {
      return openSnack("Email is invalid")
    }

    if (state.password.length < 4) {
      return openSnack("Password is not long enough")
    }

    if (state.re_password !== state.password) {
      return openSnack("Passwords are not the same!");
    }

    if (state.question.length < 10) {
      return openSnack("Question is not long enough!");
    }

    if (state.answer.length < 4) {
      return openSnack("Answer should be at least 4 character-long");
    }

    const res = await register({
      firstName: state.first_name,
      lastName: state.last_name,
      email: state.email,
      password: state.password,
      question: state.question,
      answer: state.answer,
    });

    if (res.id) {
      setUser(res)
    } else {
      return openSnack(res.message? res.message: res);
    }

    history.push("/app");
  };

  const onInputChange = (name, event) => {
    setState({ ...state, [name]: event.target.value});
  };

  return (
    <Paper elevation={3} className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <h1 style={{ color: "rgb(181, 43, 98)" }}>Register</h1>
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="first_name"
            label="First Name"
            variant="outlined"
            fullWidth
            value={state.first_name}
            onChange={e => onInputChange("first_name", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="last_name"
            label="Last Name"
            variant="outlined"
            fullWidth
            value={state.last_name}
            onChange={e => onInputChange("last_name", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="email"
            label="Email"
            variant="outlined"
            fullWidth
            type={"email"}
            value={state.email}
            onChange={e => onInputChange("email", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="password"
            label="Password"
            variant="outlined"
            fullWidth
            type={"password"}
            value={state.password}
            onChange={e => onInputChange("password", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="re-password"
            label="Retype Password"
            variant="outlined"
            fullWidth
            type={"password"}
            value={state.re_password}
            onChange={e => onInputChange("re_password", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography>This information will be use to recover your password in the future</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="question"
            label="Security question"
            variant="outlined"
            fullWidth
            type={"text"}
            value={state.question}
            onChange={e => onInputChange("question", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="answer"
            label="Answer"
            variant="outlined"
            fullWidth
            type={"text"}
            value={state.answer}
            onChange={e => onInputChange("answer", e)}
          />
        </Grid>
        <Grid item xs={12}>
          <Button
            onClick={onRegisterClick}
            variant={"outlined"}
            style={{ backgroundColor: "rgba(0,0,0,.1)" }}
          >
            Submit
          </Button>
        </Grid>
        <Grid item xs={12} container direction={"row"} alignItems={"center"}>
          <Typography>Already have an account?</Typography>{" "}
          <Link
            style={{ marginLeft: 5, cursor: "pointer" }}
            onClick={toggleLogin}
          >
            Login
          </Link>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(styles)(RegisterComponent);
