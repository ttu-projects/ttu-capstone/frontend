export default theme => ({
  container: {
    minHeight: '80vh',
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  root:{
    width: 500,
    minHeight: 400,
    padding: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    '@media (max-width: 500px)': {
      width: '100%',
    }
  },
})
