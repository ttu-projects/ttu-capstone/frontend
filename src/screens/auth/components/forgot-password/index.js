import React, { useRef, useState } from "react";
import {
  withStyles,
  Paper,
  Grid,
  TextField,
  Button,
  Link,
  Typography
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import styles from "../styles";
import { useStoreActions } from "easy-peasy";
import {
  getQuestion,
  verifySecurityQuestion,
  resetPassword
} from "../../../../services/auth";

const ForgotPasswordComponent = ({ classes, toggleLogin }) => {
  const history = useHistory();
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const [reset, setReset] = useState(false);
  const [question, setQuestion] = useState(null);
  const [email, setEmail] = useState("");
  const emailRef = useRef();
  const answerRef = useRef();
  const passwordRef = useRef();
  const retypeRef = useRef();

  const submitEmailAddress = async () => {
    if (emailRef.current.value.length < 5) {
      emailRef.current.focus();
      return openSnack("Email is invalid!");
    }

    const res = await getQuestion({ email: emailRef.current.value });

    setEmail(emailRef.current.value);
    if (res.question) {
      setQuestion(res.question);
    } else {
      return openSnack(res.message ? res.message : res);
    }
  };

  const onForgotPassword = async () => {
    if (answerRef.current.value === "") {
      answerRef.current.focus();
      return openSnack("Email address is invalid!");
    }

    const res = await verifySecurityQuestion({
      email,
      answer: answerRef.current.value
    });
    if (res.success) {
      setReset(true);
    } else {
      return openSnack(res.message ? res.message : "Invalid answer!");
    }
  };

  const onResetPassword = async () => {
    if (passwordRef.current.value.length < 6) {
      passwordRef.current.focus();
      return openSnack("Password is incorect!");
    }

    if (passwordRef.current.value !== retypeRef.current.value) {
      retypeRef.current.focus();
      return openSnack("Passwords don't match!");
    }

    const res = await resetPassword({
      email,
      password: passwordRef.current.value
    });

    if (res.success) {
      history.push("/login");
    } else {
      return openSnack(res.message ? res.message : res);
    }
    history.push("/app");
  };

  return (
    <div className={classes.container}>
      <Paper elevation={3} className={classes.root}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <h1 style={{ color: "rgb(181, 43, 98)" }}>Forgot password</h1>
          </Grid>
          {!reset ? (
            !question ? (
              <Grid item xs={12}>
                <TextField
                  id="email"
                  inputRef={emailRef}
                  label="Email"
                  variant="outlined"
                  fullWidth
                  required
                  type={"email"}
                />
              </Grid>
            ) : (
              <>
                <Grid item xs={12}>
                  <TextField
                    id="Question"
                    label="Question"
                    variant="outlined"
                    fullWidth
                    disabled={true}
                    value={question}
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    id="Answer"
                    label="Answer"
                    variant="outlined"
                    fullWidth
                    inputRef={answerRef}
                  />
                </Grid>
              </>
            )
          ) : (
            <>
              <Grid item xs={12}>
                <TextField
                  id="password"
                  inputRef={passwordRef}
                  label="Password"
                  variant="outlined"
                  type="password"
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  id="retype"
                  inputRef={retypeRef}
                  label="Retype Password"
                  variant="outlined"
                  type="password"
                  fullWidth
                />
              </Grid>
            </>
          )}

          <Grid item xs={12}>
            <Button
              style={{ backgroundColor: "rgba(0,0,0,.1)" }}
              onClick={
                !reset
                  ? question
                    ? onForgotPassword
                    : submitEmailAddress
                  : onResetPassword
              }
              variant={"outlined"}
            >
              Submit
            </Button>
          </Grid>
        </Grid>
      </Paper>
    </div>
  );
};

export default withStyles(styles)(ForgotPasswordComponent);
