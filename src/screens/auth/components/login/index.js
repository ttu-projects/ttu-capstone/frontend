import React, { useRef } from "react";
import {
  withStyles,
  Paper,
  Grid,
  TextField,
  Button,
  Link,
  Typography
} from "@material-ui/core";
import { useHistory } from "react-router-dom";

import styles from "../styles";
import { useStoreActions } from "easy-peasy";
import {login} from "../../../../services/auth";

const LogInComponent = ({ classes, toggleLogin }) => {
  const history = useHistory();
  const setUser = useStoreActions(actions => actions.auth.setUser);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const emailRef = useRef();
  const passwordRef = useRef();

  const forgotPassword = () => {
    history.push('forgot-password');
  };

  const onLoginClick = async () => {
    if (emailRef.current.value === "") {
      emailRef.current.focus();
      return openSnack("Email address is invalid!");
    }

    if (passwordRef.current.value.length < 6) {
      passwordRef.current.focus();
      return openSnack("Password is incorect!");
    }
    const res = await login(emailRef.current.value, passwordRef.current.value);
    if (res.id) {
      setUser(res);
    } else {
      return openSnack(res.message? res.message: res);
    }
    history.push("/app");
  };

  return (
    <Paper elevation={3} className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <h1 style={{ color: "rgb(181, 43, 98)" }}>Login</h1>
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="email"
            inputRef={emailRef}
            label="Email"
            variant="outlined"
            fullWidth
            required
            type={"email"}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="password"
            inputRef={passwordRef}
            label="Password"
            variant="outlined"
            type="password"
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <Link
            style={{ marginLeft: 5, cursor: "pointer" }}
            onClick={forgotPassword}
          >
           Forgot Password
          </Link>
        </Grid>
        <Grid item xs={12}>
          <Button
            style={{ backgroundColor: "rgba(0,0,0,.1)" }}
            onClick={onLoginClick}
            variant={"outlined"}
          >
            Submit
          </Button>
        </Grid>
        <Grid item xs={12} container direction={"row"} alignItems={"center"}>
          <Typography>{"New to Knotty Cash? "}</Typography>
          <Link
            style={{ marginLeft: 5, cursor: "pointer" }}
            onClick={toggleLogin}
          >
            Register
          </Link>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(styles)(LogInComponent);
