import React, { useState } from "react";
import { withStyles } from "@material-ui/core";
import LogInComponent from "./components/login";
import RegisterComponent from "./components/register";

const AuthPage = ({classes}) => {
  const [isLogin, setIsLogin] = useState(true);

  const toggleLogin = () =>  setIsLogin(!isLogin);

  return (
    <div className={classes.root}>
      {isLogin ? <LogInComponent toggleLogin={toggleLogin} /> : <RegisterComponent toggleLogin={toggleLogin} />}
    </div>
  );
};

export default withStyles(theme => ({
  root: {
    minHeight: '80vh',
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}))(AuthPage);
