import React, { useCallback, useState } from "react";
import {
  Grid,
  withStyles,
  Divider,
  Typography,
  Paper,
  List,
  ListItemText,
  ListItem,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Tabs,
  Tab,
  LinearProgress,
  Button,
  Modal
} from "@material-ui/core";

import {
  ArrowDownward,
  ArrowUpward,
} from "@material-ui/icons";

import Chart from "react-apexcharts";

import { useStoreActions, useStoreState } from "easy-peasy";

import TradeScreen from "../trade";
import Transactions from "../transactions";
import StockDetail from "../trade/components/StockDetail";
import { getStocks, sellStock } from "../../services/api";
import { randomNumber } from "../../services";

const InvestmentPortfolio = ({ classes }) => {
  const {
    investmentPortfolio: { holdings: ownedStocks, ...investmentPortfolio },
    transactions,
    ...user
  } = useStoreState(state => state.auth.user);
  const [tabIdx, setTabIdx] = useState(0);
  const [news, setNews] = useState(null);
  const [stocks, setStocks] = useState(null);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const setUser = useStoreActions(actions => actions.auth.setUser);
  const [stateChange, setStateChange] = useState(false);

  const [selectedStock, setSelectedStock] = useState(null);
  const [totalGain, setTotalGain] = useState(0);

  const [percentGain, setPercentGain] = useState(0);

  const [chartState, setChartState] = useState({
    series: ownedStocks
      ? [
          investmentPortfolio.cashAvailable,
          ...ownedStocks.map(stock => stock.currentPrice)
        ]
      : [investmentPortfolio.cashAvailable],
    options: {
      chart: {
        type: "donut"
      },
      labels: ownedStocks
        ? ["Cash", ...ownedStocks.map(stock => stock.name)]
        : ["Cash"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    }
  });

  React.useEffect(() => {
    let series = ownedStocks?.map(stock => typeof stock.currentPrice === 'string'? parseFloat(stock.currentPrice) : stock.currentPrice) || [];
    let labels = ownedStocks?.map(stock => stock.name) || [];

    if (investmentPortfolio) {
      series = [investmentPortfolio.cashAvailable, ...series];
      labels = ["Cash", ...labels];
    }

    setChartState(prevState => ({
      series,
      options: { ...prevState.options, labels }
    }));
  }, [stateChange]);

  React.useEffect(() => {
    if (ownedStocks) {
      let gain = 0;
      let total = 0;

      for (let stock of ownedStocks) {
        gain += parseFloat(stock.currentPrice) - stock.priceAquired;
        total += stock.priceAquired;
      }

      setTotalGain(gain);
      setPercentGain(gain/(total+ investmentPortfolio.cashAvailable));
    }
  }, [stocks]);

  React.useEffect(() => {
    if (stocks) {
      const _stocks = [...stocks];
      const _stock = _stocks[Math.round(Math.random() * (stocks.length - 1))];
      _stock.currentPrice = (_stock.price + randomNumber()).toFixed(2);

      _stock.confidenceInterval = randomNumber().toFixed(2);

      const timeoutId = setTimeout(
        () => setStocks(_stocks),
        Math.round(Math.random() * 4000)
      );

      return () => clearTimeout(timeoutId);
    }
  }, [stocks]);

  React.useEffect(() => {
    fetch(
      "https://api.nytimes.com/svc/search/v2/articlesearch.json?q=stocks&api-key=kSO5fp91G6AgxfwKogojmWl2G3wvw3jM"
    )
      .then(data => data.json())
      .then(data => {
        setNews(data.response ? data.response.docs : []);
      })
      .catch(err => setNews([]));

    getStocks()
      .then(async data => {
        for (let stock of data) {
          stock.currentPrice = stock.price;
          stock.confidenceInterval = randomNumber().toFixed(2);
        }
        setStocks(data);
      })
      .catch(err => console.log(err));
  }, []);

  const handleChange = (event, value) => {
    setTabIdx(value);
  };

  const onSellStock = async (stock, amount) => {
    try {
      if (amount <= 0) {
        return openSnack("Amount must be a positive number!");
      }

      if (amount > parseInt(stock.amount)) {
        return openSnack("Can't sell more than you have!");
      }
      const res = await sellStock({ ...stock, amount });
      setUser(res);
      setStateChange(prevState => !prevState);
      openSnack("Successfully sold stock!");
    } catch (e) {
      openSnack(e?.message || e);
    }
  };

  const renderTabs = () => {
    switch (tabIdx) {
      case 0:
        return renderStocks();
      case 1:
        return <TradeScreen stocks={stocks} setStateChange={setStateChange} />;
      case 2:
        return <Transactions transactions={transactions || []} />;
      default:
        return null;
    }
  };

  const renderNews = () => {
    return news ? (
      news.length > 0 ? (
        <List>
          {news.map(_news => (
            <Button
              key={_news.abstract}
              onClick={() => {
                window.open(_news.web_url);
              }}
            >
              <ListItem alignItems={"flex-start"}>
                <ListItemText
                  primary={<h4 style={{ margin: 0 }}>{_news.headline.main}</h4>}
                  secondary={
                    <React.Fragment>
                      {_news.headline.print_headline}
                      <br />
                      <i>{_news.source}</i>
                    </React.Fragment>
                  }
                />
              </ListItem>
            </Button>
          ))}
        </List>
      ) : (
        <Typography variant={"caption"}>No Content</Typography>
      )
    ) : (
      <LinearProgress />
    );
  };

  const calculateTotalGain = stock =>
    stock.currentPrice * stock.amount - stock.priceAquired * stock.amount;

  const calculatePercentGain = stock =>
    calculateTotalGain(stock) / (stock.priceAquired * stock.amount);

  const renderStocks = () => {
    return !ownedStocks || ownedStocks.length === 0 ? (
      <Paper className={classes.stocks}>
        <Typography variant={"caption"}>No Stocks!</Typography>
      </Paper>
    ) : (
      <Grid container spacing={3}>
        <Grid item xs={12}>
          <Divider />
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Symbol</TableCell>
                  <TableCell align="right">Name</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Current Price</TableCell>
                  <TableCell align="right">Price Acquired</TableCell>
                  <TableCell align="right">Total Gain</TableCell>
                  <TableCell align="right">Percent Gain</TableCell>
                  <TableCell align="right">Total Value</TableCell>
                  <TableCell align="right">Total Cost</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {ownedStocks.map(stock => {
                  if (stocks){
                    stock.currentPrice = stocks.filter(s => s.symbol === stock.symbol)[0].currentPrice;
                    stock.confidenceInterval = stocks.filter(s => s.symbol === stock.symbol)[0].confidenceInterval;
                  }
                  return (
                    <TableRow
                      key={stock.symbol}
                      style={{ cursor: "pointer" }}
                      onClick={() => setSelectedStock(stock)}
                    >
                      <TableCell component="th" scope="row">
                        {stock.symbol}
                      </TableCell>
                      <TableCell align="right">{stock.name}</TableCell>
                      <TableCell align="right">{stock.amount}</TableCell>
                      <TableCell align="right"><span>
                        {stock.currentPrice < stock.priceAquired && <ArrowDownward />}
                        {stock.currentPrice > stock.priceAquired && <ArrowUpward />}
                        ${stock.currentPrice}
                      </span></TableCell>
                      <TableCell align="right">${stock.priceAquired}</TableCell>
                      <TableCell align="right">
                        {calculateTotalGain(stock).toFixed(2)}
                      </TableCell>
                      <TableCell align="right">
                        {calculatePercentGain(stock).toFixed(2)}
                      </TableCell>
                      <TableCell align="right">
                        {(stock.amount * stock.currentPrice).toFixed(2)}
                      </TableCell>
                      <TableCell align="right">
                        {(stock.amount * stock.priceAquired).toFixed(2)}
                      </TableCell>
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
    );
  };

  return (
    <Paper className={classes.root}>
      <Grid container>
        <Grid item xs={12}>
          <Divider />
        </Grid>
        <Grid item xs={12} container spacing={3} className={classes.content}>
          <Grid item xs={12} container spacing={3}>
            <Grid item sm={4} xs={12}>
              <Paper className={classes.subSection}>
                <h2>${investmentPortfolio?.cashAvailable.toFixed(2) || 0}</h2>
                <Typography variant={"caption"}>Cash Available</Typography>
              </Paper>
            </Grid>
            <Grid item sm={4} xs={12}>
              <Paper className={classes.subSection}>
                <h2>${totalGain.toFixed(2) || 0}</h2>
                <Typography variant={"caption"}>Total Gain Loss</Typography>
              </Paper>
            </Grid>
            <Grid item sm={4} xs={12}>
              <Paper className={classes.subSection}>
                <h2>{(percentGain * 100).toFixed(0) || 0}%</h2>
                <Typography variant={"caption"}>Percent Gain Loss</Typography>
              </Paper>
            </Grid>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper className={classes.section}>{renderNews()}</Paper>
          </Grid>
          <Grid item xs={12} md={6}>
            <Paper className={classes.section}>
              <Chart
                options={chartState.options}
                series={chartState.series}
                type={"donut"}
                height={450}
              />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper square>
              <Tabs
                value={tabIdx}
                indicatorColor="primary"
                textColor="primary"
                onChange={handleChange}
                aria-label="disabled tabs example"
                centered
              >
                <Tab label="Owned Stocks" />
                <Tab label="Trade Stocks" />
                <Tab label="Transactions" />
              </Tabs>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            {renderTabs()}
          </Grid>
        </Grid>
      </Grid>
      <Modal open={Boolean(selectedStock)}>
        {selectedStock ? (
          <StockDetail
            sell
            onSellStock={onSellStock}
            onClose={() => setSelectedStock(null)}
            stock={selectedStock}
          />
        ) : (
          <Typography>Loading...</Typography>
        )}
      </Modal>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5,
    minHeight: "100vh",
    backgroundColor: "rgba(0,0,0,.1)"
  },
  content: {
    marginTop: 20
  },
  section: {
    boxSizing: "border-box",
    padding: 20,
    height: 500,
    overflow: "scroll"
  },
  subSection: {
    boxSizing: "border-box",
    padding: 20,
    minHeight: "15vh",
    backgroundImage: `url(${window.location.origin}/images/graph.png)`,
    backgroundPosition: "bottom",
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    "& h2": {
      margin: 0,
      padding: 0
    }
  },
  stocks: {
    minHeight: "20vh",
    padding: 20,
    display: "flex",
    justifyContent: "center"
  }
}))(InvestmentPortfolio);
