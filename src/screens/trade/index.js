import React from "react";
import {
  Divider,
  Grid,
  withStyles,
  Collapse,
} from "@material-ui/core";
import BuyScreen from "./components/BuyScreen";

const TradeScreen = ({ classes, stocks, setStateChange }) => {
  return (
    <Grid container>
      <Grid
        item
        xs={12}
        container
        direction={"row"}
        justify={"space-between"}
        alignItems={"center"}
      >
        <Divider />
      </Grid>
      <Grid item xs={12} container className={classes.content}>
        <Grid item xs={12}>
          <Collapse in={true}>
            <BuyScreen stocks={stocks} setStateChange={setStateChange}/>
          </Collapse>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5,
    minHeight: "20vh"
  },
  content: {
    display: "flex",
    justifyContent: "center"
  }
}))(TradeScreen);
