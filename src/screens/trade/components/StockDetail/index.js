import React, { useState } from "react";
import {
  Paper,
  Grid,
  Typography,
  Divider,
  withStyles,
  IconButton,
  Button,
  Popover,
  CircularProgress
} from "@material-ui/core";
import {
  ArrowBackIos,
  ArrowDownward,
  ArrowUpward,
  Spin
} from "@material-ui/icons";

import Chart from "react-apexcharts";

import BuyNow from "./components/BuyPopup";
import SellNow from "./components/SellPopup";
import StyledLabel from "../../../../components/StyledLabel";

const StockDetail = ({
  classes,
  stock,
  sell,
  onClose,
  onBuyStock,
  onSellStock
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [series, setSeries] = useState(null);

  React.useEffect(() => {
    const data = [];
    if (stock?.history) {
      for (let history of stock.history) {
        data.push({
          x: history.tm,
          y: [
            parseFloat(history.o).toFixed(2),
            parseFloat(history.h).toFixed(2),
            parseFloat(history.l).toFixed(2),
            parseFloat(history.c).toFixed(2)
          ]
        });
      }
    }

    setSeries([{ data }]);
  }, [stock]);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const plotOptions = {
    chart: {
      type: "candlestick",
      height: 350
    },
    title: {
      text: "CandleStick Chart",
      align: "left"
    },
    xaxis: {
      type: "datetime"
    },
    yaxis: {
      tooltip: {
        enabled: true
      }
    }
  };

  return (
    <Paper className={classes.root}>
      <Grid container spacing={3}>
        <Grid
          item
          xs={12}
          container
          spacing={3}
          direction={"row"}
          alignItems={"center"}
        >
          <IconButton onClick={onClose}>
            <ArrowBackIos />
          </IconButton>
          <Typography variant={"h6"}>{`${sell ? "Selling" : "Buying"} ${
            stock.name
          } stock`}</Typography>
          <Divider />
        </Grid>
        <Grid item container direction={"row"} alignItems={"center"} xs={12}>
          <Grid item xs={3} style={{ height: "100%" }}>
            <Paper className={classes.info}>
              <StyledLabel label={"Symbol"} text={`$${stock.symbol}`} />
              <StyledLabel label={"Current Price"} text={`$${stock?.price || stock.currentPrice}`} />
              {sell && (
                <StyledLabel label={"Amount"} text={`${stock.amount}`} />
              )}
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <StyledLabel label={"Confidence Index"} />
                <span
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    color:
                      stock.confidenceInterval === 0
                        ? "black"
                        : stock.confidenceInterval < 0
                        ? "red"
                        : "blue"
                  }}
                >
                  {stock.confidenceInterval}
                </span>
              </div>
            </Paper>
          </Grid>
          <Grid
            item
            xs={9}
            style={{
              height: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            {series ? (
              <Chart
                options={plotOptions}
                series={series}
                type={"candlestick"}
                height={350}
                style={{ height: "100%", width: "100%" }}
              />
            ) : (
              <CircularProgress color={"secondary"} />
            )}
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Button variant={"contained"} color={"primary"} onClick={handleClick}>
            {sell ? "Sell" : "Buy"}
          </Button>
          <Popover
            open={Boolean(anchorEl)}
            anchorEl={anchorEl}
            onClose={handleClose}
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "center"
            }}
            transformOrigin={{
              vertical: "top",
              horizontal: "center"
            }}
          >
            {sell ? (
              <SellNow
                stock={stock}
                onSellStock={(stock, amount) => {
                  onSellStock(stock, amount);
                  onClose();
                }}
              />
            ) : (
              <BuyNow
                stock={stock}
                onBuyStock={(stock, amount) => {
                  onBuyStock(stock, amount);
                  onClose();
                }}
              />
            )}
          </Popover>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5,
    minHeight: "100vh"
  },
  info: {
    padding: 20,
    height: "100%",
    boxSizing: "border-box"
  }
}))(StockDetail);
