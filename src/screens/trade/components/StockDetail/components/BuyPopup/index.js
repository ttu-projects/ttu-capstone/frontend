import React, { useState } from "react";
import { withStyles, Button, Paper } from "@material-ui/core";
import StyledInput from "../../../../../../components/StyledInput";
import StyledLabel from "../../../../../../components/StyledLabel";

const BuyNow = ({ classes, stock, onBuyStock }) => {
  const [amount, setAmount] = useState(0);

  return (
    <Paper className={classes.root}>
      <StyledInput
        type={"number"}
        label={"Amount of share"}
        value={amount}
        onChange={e => {
          setAmount(e.target.value);
        }}
      />
      <StyledLabel label={"Current Price:"} text={"$" + stock.price} />
      <StyledLabel
        label={"Total Price:"}
        text={"$" + Math.max(0, (stock.price * amount).toFixed(2))}
      />
      <StyledLabel
        label={"Confidence index:"}
        text={(stock.confidenceInterval * 100).toString() + "%"}
      />
      <Button
        variant={"contained"}
        onClick={() => {
          stock.acquiredPrice = stock.price;
          onBuyStock(stock, amount);
        }}
      >
        Buy Now
      </Button>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    margin: 5
  }
}))(BuyNow);
