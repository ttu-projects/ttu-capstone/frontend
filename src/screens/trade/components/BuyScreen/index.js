import React, { useEffect, useState } from "react";
import {
  Paper,
  List,
  Link,
  withStyles,
  Grid,
  Typography,
  Divider,
  Modal,
  Button,
  Popover,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody
} from "@material-ui/core";
import StockDetail from "../StockDetail";
import BuyNow from "../StockDetail/components/BuyPopup";

import { ArrowUpward, ArrowDownward } from "@material-ui/icons";

import {useStoreActions, useStoreState} from "easy-peasy";
import { buyStock } from "../../../../services/api";

const BuyScreen = ({ classes, stocks, setStateChange }) => {
  const [selectedStock, setSelectedStock] = useState(null);
  const [buyingStock, setBuyingStock] = useState({});
  const [anchorEl, setAnchorEl] = React.useState(null);
  const openSnack = useStoreActions(actions => actions.snack.openSnack);
  const setUser = useStoreActions(actions => actions.auth.setUser);
  const user = useStoreState(state => state.auth.user);

  const handleClick = stock => event => {
    setBuyingStock(stock);
    setAnchorEl(event.currentTarget);
  };

  React.useEffect(() => {
    if (buyingStock && stocks) {
      setBuyingStock(
        stocks.filter(stock => stock.symbol === buyingStock.symbol)[0]
      );
    }
  }, [buyingStock, stocks]);

  React.useEffect(() => {
    if (selectedStock && stocks) {
      setSelectedStock(
        stocks.filter(stock => stock.symbol === selectedStock.symbol)[0]
      );
    }
  }, [selectedStock, stocks]);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onBuyStock = async (stock, amount) => {
    try {
      if (amount <= 0) {
        return openSnack("Amount must be a positive number!");
      }

      if (amount * stock.currentPrice > user?.investmentPortfolio?.cashAvailable) {
        return openSnack("Not enough cash");
      }

      const res = await buyStock({ ...stock, amount });
      setUser(res);
      setStateChange(prevState => !prevState);
      setAnchorEl(null);
      openSnack("Successfully bought the Stock");
    } catch (e) {
      openSnack(e?.message || e);
    }
  };

  return (
    <Paper className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} container justify={"center"} alignItems={"center"}>
          {stocks ? (
            stocks.length > 0 ? (
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Divider />
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Symbol</TableCell>
                          <TableCell align="right">Name</TableCell>
                          <TableCell align="right">Current Price</TableCell>
                          <TableCell align="right">
                            Confidence Interval
                          </TableCell>
                          <TableCell align="right" />
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {stocks.map(stock => (
                          <TableRow
                            key={stock.symbol}
                            style={{ cursor: "pointer" }}
                          >
                            <TableCell component="th" scope="row">
                              {stock.symbol}
                            </TableCell>
                            <TableCell align="right">
                              <Link onClick={() => setSelectedStock(stock)}>
                                {stock.name}
                              </Link>
                            </TableCell>
                            <TableCell align="right">
                              <span>
                                {stock.currentPrice < stock.price && (
                                  <ArrowDownward />
                                )}
                                {stock.currentPrice > stock.price && (
                                  <ArrowUpward />
                                )}
                                ${stock.currentPrice}
                              </span>
                            </TableCell>
                            <TableCell align="right">
                              <span
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                  justifyContent: "center",
                                  color:
                                    stock.confidenceInterval === 0
                                      ? "black"
                                      : stock.confidenceInterval < 0
                                      ? "red"
                                      : "blue"
                                }}
                              >
                                {stock.confidenceInterval}
                              </span>
                            </TableCell>
                            <TableCell align="right">
                              <Button
                                style={{ marginLeft: 10 }}
                                onClick={handleClick(stock)}
                                variant={"outlined"}
                              >
                                Buy
                              </Button>
                              <Popover
                                open={Boolean(anchorEl)}
                                anchorEl={anchorEl}
                                onClose={handleClose}
                                anchorOrigin={{
                                  vertical: "bottom",
                                  horizontal: "center"
                                }}
                                transformOrigin={{
                                  vertical: "top",
                                  horizontal: "center"
                                }}
                              >
                                <BuyNow
                                  stock={buyingStock}
                                  onBuyStock={onBuyStock}
                                />
                              </Popover>
                            </TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </Grid>
              </Grid>
            ) : (
              <Typography variant={"caption"}>No stocks</Typography>
            )
          ) : (
            <Typography>Loading...</Typography>
          )}
          <Modal open={Boolean(selectedStock)}>
            {selectedStock ? (
              <StockDetail
                onBuyStock={onBuyStock}
                onClose={() => setSelectedStock(null)}
                stock={selectedStock}
              />
            ) : (
              <Typography>Loading...</Typography>
            )}
          </Modal>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default withStyles(theme => ({
  root: {
    padding: 20,
    height: "100%"
  }
}))(BuyScreen);
