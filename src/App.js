import React from "react";
import "./App.css";
import { BrowserRouter } from "react-router-dom";
import Routes from "./Routes";
import { StoreProvider } from "easy-peasy";
import { StripeProvider } from "react-stripe-elements";

import store from "./store";
import Layout from "./screens/layout";

function App() {

  return (
    <StoreProvider store={store}>
      <BrowserRouter>
        <StripeProvider apiKey={"pk_test_xzEE6iRxENS696NOE06jCUUc"}>
          <Layout>
            <Routes />
          </Layout>
        </StripeProvider>
      </BrowserRouter>
    </StoreProvider>
  );
}

export default App;
